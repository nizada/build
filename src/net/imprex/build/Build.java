package net.imprex.build;

import org.bukkit.plugin.java.JavaPlugin;

import net.imprex.build.command.COMMAND_GameMode;
import net.imprex.build.command.COMMAND_GiveItem;
import net.imprex.build.command.COMMAND_Head;
import net.imprex.build.command.COMMAND_Ping;
import net.imprex.build.listener.PlayerChat;
import net.imprex.build.listener.PlayerConnection;
import net.imprex.build.listener.PlayerDead;

public class Build extends JavaPlugin {
	
	public static final String PREFIX = "�8[�7Build�8] ";
	
	public static Build instance;
	
	public Build() {
		instance = this;
	}
	
	public void onEnable() {
		
		registerCommands();
		registerListener();
	}
	
	public void onDisable() {};
	
	private void registerListener() {
		getServer().getPluginManager().registerEvents(new PlayerChat(), this);
		getServer().getPluginManager().registerEvents(new PlayerConnection(), this);
		getServer().getPluginManager().registerEvents(new PlayerDead(), this);
	}
	
	private void registerCommands() {
		getCommand("ping").setExecutor(new COMMAND_Ping());
		getCommand("gamemode").setExecutor(new COMMAND_GameMode());
		getCommand("head").setExecutor(new COMMAND_Head());
		getCommand("giveitem").setExecutor(new COMMAND_GiveItem());
	}
}