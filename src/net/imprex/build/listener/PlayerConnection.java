package net.imprex.build.listener;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerConnection implements Listener {
	
	@EventHandler
	public void onChat(PlayerJoinEvent event) throws InterruptedException {
		event.setJoinMessage("�8[�a+�8] �7" + event.getPlayer().getName());
		event.getPlayer().setGameMode(GameMode.CREATIVE);
		
		event.getPlayer().teleport(new Location(Bukkit.getWorld("build"), -35.5, 76.2, -35.5));
	}
	
	@EventHandler
	public void onChat(PlayerQuitEvent event) {
		event.setQuitMessage("�8[�c-�8] �7" + event.getPlayer().getName());
	}
}