package net.imprex.build.command;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.Main;
import org.bukkit.entity.Player;

public class COMMAND_Home implements CommandExecutor {

	private Main plugin;
	File file = new File("plugins/home", "homes.yml");
	FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

	public void home(Main home) {
		plugin = home;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("�aDu musst ein Spieler sein.");
			return true;
		}
		Player player = (Player) sender;
		
		if (args.length == 1) {
			String homeName = args[0].toLowerCase();
			
			if(!cfg.contains(player.getName() + "." + homeName)) {
				player.sendMessage("�cDu besitzt kein Home mit denn namen �7\"�4" + homeName + "�7\"�c.");
				return true;
			}
			
			try {
				cfg.load(file);
			} catch (IOException | InvalidConfigurationException e) {
				e.printStackTrace();
			}
			
			String world = cfg.getString(player.getName() + "." + homeName + ".world");
			double x = cfg.getDouble(player.getName() + "." + homeName + ".x");
			double y = cfg.getDouble(player.getName() + "." + homeName + ".y");
			double z = cfg.getDouble(player.getName() + "." + homeName + ".z");
			double yaw = cfg.getDouble(player.getName() + "." + homeName + ".yaw");
			double pitch = cfg.getDouble(player.getName() + "." + homeName + ".pitch");
			Location loc = new Location(Bukkit.getWorld(world), x, y, z);
			loc.setPitch((float) pitch);
			loc.setYaw((float) yaw);
			
			player.teleport(loc);
			player.sendMessage("�b Du wurdest zu deinem Home: �c" + args[0] + " �bteleportiert!");
		} else {
			player.sendMessage("�c/home <name>");
		}
		return true;
	}
}